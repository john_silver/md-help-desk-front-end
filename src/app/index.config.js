export function config ($logProvider, toastrConfig, $mdThemingProvider, tmhDynamicLocaleProvider, $httpProvider, envServiceProvider) {
  'ngInject';
  // Enable log
  $logProvider.debugEnabled(true);

  $httpProvider.defaults.timeout = 90000000000;

  // Set options third-party lib
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.progressBar = true;

  //tmhDynamicLocaleProvider.set('ru');
  tmhDynamicLocaleProvider.localeLocationPattern('https://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.5.8/angular-locale_ru-ru.js');
  $mdThemingProvider.theme('default')
    .primaryPalette('blue');


  // set the domains and variables for each environment
  envServiceProvider.config({
    domains: {
      development: ['localhost'],
      production: []
    },
    vars: {
      development: {
        apiUrl: 'http://localhost:8081/'
      },
      production: {
        apiUrl: 'http://128.199.52.120:8081/'
      }
    }
  });

  envServiceProvider.check();

}
