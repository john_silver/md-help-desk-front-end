export class TicketNewController{
  constructor(CheckAuthService, $http,toastr, $localStorage, $state,
              envService, $stateParams, TicketSupportService, $sce){
    'ngInject';
    this.$http = $http;
    this.toastr = toastr;
    this.envService = envService;
    this.$localStorage = $localStorage;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.TicketSupportService = TicketSupportService;
    this.$sce = $sce;
    var vm = this;
    vm.logout = CheckAuthService.logout;

    vm.ticket = {};
    vm.ticket.attachments = [];

    this.replyToClient = (body)=>{
      let userId = this.$localStorage.user._id;

      $http({
        url : envService.read('apiUrl')+"api/ticket/sendMail?userId="+userId,
        method : "POST",
        data : body
      }).then(function successCallback(response) {
        console.log(response);
        $state.go('ticket');

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };


  }


}
