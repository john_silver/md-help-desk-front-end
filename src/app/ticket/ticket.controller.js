export class TicketController{
  constructor(CheckAuthService, $http,toastr, $localStorage, $state, envService, TicketSupportService, ngProgressFactory, $location, $stateParams,$scope) {
    'ngInject';
    this.$http = $http;
    this.toastr = toastr;
    this.envService = envService;
    this.$localStorage = $localStorage;
    this.$state = $state;
    this.TicketSupportService = TicketSupportService;
    var vm = this;
    vm.logout = CheckAuthService.logout;

    console.log($location.search());
    vm.filterStatus = $location.search().status;
    if ($stateParams.status) vm.filterStatus = $stateParams.status;
    if (!vm.filterStatus) vm.filterStatus = "Открыт";
    $location.search('status',vm.filterStatus);

    this.starThread = TicketSupportService.starThread;
    this.selectorOwner = 'all';

    this.progressbar = ngProgressFactory.createInstance();
    this.progressbar.setHeight('3px');

    this.getEmployersList().then((result) => {
      this.employers = result.users.map((item)=> {
        return {
          value: item._id,
          label: `<span class="m-selected-span m-selected-face">${item.name}</span>`
        };
      });

      this.employers.unshift({
        value: null,
        label: '<span class="m-selected-span m-selected-face"></span>'
      });
      this.selectedEmployer = this.employers[0].value;
    });


    this.statuses = [
      {value: 'none', label: '<span class="m-selected-span m-selected-flag "></span>'},
      {value: 'Открыт', label: '<span class="m-selected-span">Открыт</span>'},
      {value: 'Ожидание', label: '<span class="m-selected-span">Ожидание</span>'},
      {value: 'Закрыт', label: '<span class="m-selected-span">Закрыт</span>'}
    ];
    // this.selectedEmployer = this.employers[0].value;
    this.selectedStatus = this.statuses[0].value;

    this.getTickets = (statusView, pageView)=> {
      if (statusView){
        $location.search('status',statusView);
      }
      if (pageView){
        $location.search('page',pageView);
      }
      let userId = this.$localStorage.user._id;
      console.log(vm.filterStatus);
      let status = vm.filterStatus;
      // filterByOwner
      let filterByOwner = vm.selectorOwner;
      let page =  $location.search().page;
      console.log( $location.search());
      console.log($stateParams);

      vm.progressbar.start();
      $http({
        url: `${envService.read('apiUrl')}api/thread?userId=${userId}&status=${status}&filterByOwner=${filterByOwner}&page=${page}`,
        method: "GET"
      }).then(function successCallback(response) {
        console.log(response);
        vm.progressbar.complete();
        vm.tickets = response.data.tickets;
        vm.custom.itemsCount = response.data.count;
        let ticket = vm.tickets[0];
        // $scope.$apply();
        // console.log(ticket);
        // console.log(TicketSupportService.getHeaderInObjectWithValue(ticket.lastTicket.headers, 'From'));
        // console.log(TicketSupportService.getHeaderInObjectWithValue(ticket.lastTicket.headers, 'Reply-To'));
        // console.log(TicketSupportService.getHeaderInObjectWithValue(ticket.lastTicket.headers, 'Subject'));
      }, function errorCallback(response) {
        vm.progressbar.complete();
        console.log('Ошибка запроса');
      });
    };

    this.getTickets();

    this.activatePage = (page)=> {
      this.getTickets(undefined, page);
    };

    this.custom = {
      itemsCount: 0,
      take: 10,
      activatePage: this.activatePage
    };





  }



  getEmployersList(){
    console.log('getEmployersList');
    return new Promise((resolve, reject) => {
      let userId = this.$localStorage.user._id;
      this.$http({
        url: `${this.envService.read('apiUrl')}api/user?userId=${userId}`,
        method: "GET"
      }).then((response)=> {
        console.log('return '+response);
        resolve(response.data);
      }, (response) =>{
        console.log('return err'+response);
        reject(response);
      });


    });
  }


}
