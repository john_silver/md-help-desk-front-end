export  class TicketSupportService{

  constructor ($http, $localStorage, envService){
    'ngInject';
    var self = this;

    /**
     * Функция для возврата value в массиве по name
     */
    this.getHeaderInObjectWithValue = function(object, name) {
      if (object==null){
        return null;
      }
      for (let i =0; i<object.length; i++){
        if(object[i].name == name){
          return(object[i].value);
        }
      }
      return null;
    };

    // фильтрует их строки подстроку почты в скобках '<' '>'
    this.getFilterEmail = function (str) {
      if (str==null){
        return str;
      }
      var start = str.indexOf('<');
      var finish = str.indexOf('>');
      var length = finish-start-1;
      if (~str.indexOf('<')){
        var substr = str.substr(start+1, length);
        return substr;
      }else{
        return str;
      }
    };

    this.starThread = (star, _id)=>{
      let userId = $localStorage.user._id;
      let id = _id;
      console.log(id);
      $http({
        url : envService.read('apiUrl')+"api/thread/"+id+"/?userId="+userId,
        method : "POST",
        data : {
          star
        }
      }).then(function successCallback(response) {
        console.log(response);
        console.log('update star thread');

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };

  }
}
